# xbrightness

X.org brightness control script for window managers.

## Requires

* `xrandr`
* `bash`
* GNU `awk`

**Don't forget to make the script executable!**

```
:~$ chmod +x /PATH/TO/SCRIPT/xbrightness.sh
```

## Usage

### Current brightness

```
:~$ /path/to/xbrightness.sh # no args or 'c'
```

### Increase brightness

```
:~$ /path/to/xbrightness.sh +
```

### Decrease brightness

```
:~$ /path/to/xbrightness.sh -
```

### Restore brightness

```
:~$ /path/to/xbrightness.sh r
```

### Set brightness value

```
:~$ /path/to/xbrightness.sh X.X # Where 'X.X' is any value from 0.0 up to 9.9
```

## Hacks

You can remove the `get_status` call from the options for performance:

```
case $1 in
    '+') increase;;
    '-') decrease;;
    'r') restore;;
    [0-9].[0-9]) set_brightness $1;;
    'c') get_status;;
    *) get_status
esac
```

